package io.netty.example.study.server.codec;

import io.netty.handler.codec.LengthFieldBasedFrameDecoder;

/**
 * 解决粘包和半包问题
 */
public class OrderFrameDecoder extends LengthFieldBasedFrameDecoder {

    // lengthFieldLength = 2时，需要设置initialBytesToStrip = 2
    public OrderFrameDecoder() {
        super(Integer.MAX_VALUE, 0, 2, 0, 2);
    }

}
