package io.netty.example.study.server;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioChannelOption;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.example.study.server.codec.OrderFrameDecoder;
import io.netty.example.study.server.codec.OrderFrameEncoder;
import io.netty.example.study.server.codec.OrderProtocolDecoder;
import io.netty.example.study.server.codec.OrderProtocolEncoder;
import io.netty.example.study.server.handler.AuthHandler;
import io.netty.example.study.server.handler.MetricHandler;
import io.netty.example.study.server.handler.OrderServerProcessHandler;
import io.netty.example.study.server.handler.ServerIdleCheckHandler;
import io.netty.handler.flush.FlushConsolidationHandler;
import io.netty.handler.ipfilter.IpFilterRuleType;
import io.netty.handler.ipfilter.IpSubnetFilterRule;
import io.netty.handler.ipfilter.RuleBasedIpFilter;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;
import io.netty.handler.ssl.SslContext;
import io.netty.handler.ssl.SslContextBuilder;
import io.netty.handler.ssl.util.SelfSignedCertificate;
import io.netty.handler.traffic.GlobalTrafficShapingHandler;
import io.netty.util.concurrent.DefaultThreadFactory;
import io.netty.util.concurrent.UnorderedThreadPoolEventExecutor;

import javax.net.ssl.SSLException;
import java.security.cert.CertificateException;

public class Server {

    public static void main(String[] args) throws InterruptedException, CertificateException, SSLException {
        ServerBootstrap serverBootstrap = new ServerBootstrap();
        serverBootstrap.channel(NioServerSocketChannel.class);
        // 指定线程名：new DefaultThreadFactory("boss")
        NioEventLoopGroup boss = new NioEventLoopGroup(1, new DefaultThreadFactory("boss"));
        // 跟连接数有关
        NioEventLoopGroup worker = new NioEventLoopGroup(0, new DefaultThreadFactory("worker"));
        serverBootstrap.group(boss, worker);
        // 在这里声明的，全局只有一个对象，必须指定为@ChannelHandler.Sharable共享对象
        // 不然重复加到pipeline会报错
        MetricHandler metricHandler = new MetricHandler();
        // 独立出“线程池”来处理业务
        UnorderedThreadPoolEventExecutor business = new UnorderedThreadPoolEventExecutor(10, new DefaultThreadFactory("business"));
        // 流控
        GlobalTrafficShapingHandler globalTrafficShapingHandler = new GlobalTrafficShapingHandler(new NioEventLoopGroup(), 100 * 1024 * 1024, 100 * 1024 * 1024);
        // ip白名单/黑名单
        IpSubnetFilterRule ipSubnetFilterRule = new IpSubnetFilterRule("127.0.0.1", 8, IpFilterRuleType.REJECT);
        // 授权Handler
        AuthHandler authHandler = new AuthHandler();
        // 证书
        SelfSignedCertificate selfSignedCertificate = new SelfSignedCertificate();
        System.out.println(selfSignedCertificate.certificate());
        SslContext sslContext = SslContextBuilder.forServer(selfSignedCertificate.certificate(), selfSignedCertificate.privateKey()).build();

        serverBootstrap.handler(new LoggingHandler(LogLevel.INFO));
        serverBootstrap.childHandler(new ChannelInitializer<NioSocketChannel>() {
            @Override
            protected void initChannel(NioSocketChannel ch) throws Exception {
                ChannelPipeline pipeline = ch.pipeline();
                //pipeline.addLast(new LoggingHandler(LogLevel.DEBUG));
                // 流量控制Handler
                //pipeline.addLast("TShaping", globalTrafficShapingHandler);
                // ip白名单/黑名单
                //pipeline.addLast("ipfilter", new RuleBasedIpFilter(ipSubnetFilterRule));

                // 心跳检测
                // 每10s检测是否有读到数据，如果没有，则关闭连接
                pipeline.addLast(new ServerIdleCheckHandler());
                // SSL加密
                pipeline.addLast(sslContext.newHandler(ch.alloc()));
                // Decoder过程：从上到下
                // 第一个参数是指定Handler的名称
                pipeline.addLast("orderFrameDecoder", new OrderFrameDecoder());
                pipeline.addLast(new OrderProtocolDecoder());
                // Encoder过程：从下到上
                pipeline.addLast(new OrderFrameEncoder());
                pipeline.addLast(new OrderProtocolEncoder());
                // 认证授权
                //pipeline.addLast("auth", authHandler);

                // 业务处理的Handler放在最后
                pipeline.addLast(new OrderServerProcessHandler());
                // 增强写，延迟与吞吐量的抉择
                pipeline.addLast("flushConsolidationHandler", new FlushConsolidationHandler(5 ,true));
                // 线程池处理
                //pipeline.addLast(business, new OrderServerProcessHandler());
                pipeline.addLast("metricHandler", metricHandler);
                pipeline.addLast(new LoggingHandler(LogLevel.INFO));
            }
        });

        // childOption代表实际连接
        serverBootstrap.childOption(NioChannelOption.TCP_NODELAY, true);
        serverBootstrap.option(NioChannelOption.SO_BACKLOG, 1024);

        ChannelFuture future = serverBootstrap.bind(8090).sync();
        // 统计当前eventLoop的线程数
        //System.out.println(worker.executorCount());
        future.channel().closeFuture().sync();
    }

}
