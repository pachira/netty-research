package io.netty.example.study.client.dispatcher;

import io.netty.example.study.common.OperationResult;

import java.util.HashMap;
import java.util.Map;

public class RequestPendingCenter {

    private Map<Long, OperationResultFuture> map = new HashMap<>();

    public void add(Long streamId, OperationResultFuture future) {
        map.put(streamId, future);
    }

    public void set(Long streamId, OperationResult result) {
        OperationResultFuture future = this.map.get(streamId);
        if (future != null) {
            future.setSuccess(result);
            this.map.remove(streamId);
        }
    }

}
