package io.netty.example.study.client.codec;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufAllocator;
import io.netty.channel.ChannelHandlerContext;
import io.netty.example.study.common.RequestMessage;
import io.netty.example.study.common.ResponseMessage;
import io.netty.handler.codec.MessageToMessageEncoder;

import java.util.List;

/**
 * 把RequestMessage转换为ByteBuf
 */
public class OrderProtocolEncoder extends MessageToMessageEncoder<RequestMessage> {
    @Override
    protected void encode(ChannelHandlerContext ctx, RequestMessage requestMessage, List<Object> out) throws Exception {
        ByteBuf byteBuf = ctx.alloc().buffer();
        // 重新分配buffer，可能得不到释放
        // ByteBuf byteBuf1 = ByteBufAllocator.DEFAULT.buffer();
        requestMessage.encode(byteBuf);

        out.add(byteBuf);
    }
}
